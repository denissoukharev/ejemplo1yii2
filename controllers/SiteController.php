<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
        
    }
    
    public function actionMensaje()
    {
        return $this->render('mostrarMensaje');
    }
    
    public function actionImagen()
    {
        return $this->render('imagen');
        
    }
    
    public function actionDias()
    {
        return $this->render('dias');
    }
    public function actionMeses()
    {
        return $this->render('meses');
    }
    public function actionFotos()
    {
        return $this->render('fotos');
    }
    
    public function actionFoto($id=1){
        $imagenes=[
            "1.jpg",
            "1_1.jpg",
            "2.jpg",
            "3.jpg",
            ];
        echo $imagenes[$id];
        
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    

    /**
     * Logout action.
     *
     * @return Response
     */
    

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
   

    /**
     * Displays about page.
     *
     * @return string
     */
   
}
